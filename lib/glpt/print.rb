module Glpt
  def self.print_issue_header
    puts "Id\tÉtat\tTitre\tVersion cible\tTemps estimé\tTemps passé"
  end

  def self.print_issue i
    milestone = i.milestone.nil? ? '' : i.milestone.title
    puts "#{i.iid}\t#{i.state}\t#{i.title}\t#{milestone}\t#{i.time_stats.human_time_estimate}\t#{i.time_stats.human_total_time_spent}"
  end
end
