require 'gitlab'

module Glpt
  def self.issues(project_path, label)
    all = Gitlab.issues(project_path, per_page: 50)
    all.select do |i|
      i.labels.include?label
    end
  end
end
